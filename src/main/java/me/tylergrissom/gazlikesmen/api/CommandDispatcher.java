package me.tylergrissom.gazlikesmen.api;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (c) 2013-2017 Tyler Grissom
 */
public class CommandDispatcher {

    private CommandSender commandSender;

    public CommandSender getCommandSender() {
        return commandSender;
    }

    public CommandDispatcher() {
        this.commandSender = Bukkit.getConsoleSender();
    }

    private void dispatchCommand(String command) {
        Bukkit.dispatchCommand(getCommandSender(), command);
    }

    public void dispatchPlayerCommand(String command, Player player) {
        String str = command.replaceAll("%player%", player.getName());

        dispatchCommand(str);
    }
}
