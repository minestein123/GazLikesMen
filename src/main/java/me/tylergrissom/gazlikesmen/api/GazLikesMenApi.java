package me.tylergrissom.gazlikesmen.api;

import me.tylergrissom.gazlikesmen.GazLikesMenPlugin;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Copyright (c) 2013-2017 Tyler Grissom
 */
public class GazLikesMenApi {

    public class Animation extends BukkitRunnable {

        private GazLikesMenPlugin plugin;
        private Player player;
        private int cycle, slot;

        public Animation(GazLikesMenPlugin plugin, Player player) {
            this.plugin = plugin;
            this.player = player;
            this.cycle = 0;
            this.slot = 0;
        }

        private void set(int slot, ItemStack itemStack) {
            player.getInventory().setItem(slot, itemStack);
        }

        public void run() {
            if (cycle == 0) {
                stashInv(player);

                cycle++;

                return;
            }

            if (cycle == 4) {
                cancel();

                return;
            }

            if (slot == 9) {
                slot = 0;
                cycle++;

                for (int i = 0; i < 8; i++) {
                    set(i, null);
                }

                return;
            }

            if (cycle == 1) {
                List<Reward> rewards = getLoadedRewards();

                for (int i = 0; i < 8; i++) {
                    set(i, rewards.get(i).getIcon());
                }

                slot++;
            }
        }
    }

    private GazLikesMenPlugin plugin;
    private CommandDispatcher commandDispatcher;
    private List<Reward> loadedRewards;
    private Map<UUID, PlayerInventory> cachedInventories;

    public GazLikesMenPlugin getPlugin() {
        return plugin;
    }

    public CommandDispatcher getCommandDispatcher() {
        return commandDispatcher;
    }

    public List<Reward> getLoadedRewards() {
        return loadedRewards;
    }

    public Map<UUID, PlayerInventory> getCachedInventories() {
        return cachedInventories;
    }

    public GazLikesMenApi(GazLikesMenPlugin plugin) {
        this.plugin = plugin;
        this.commandDispatcher = new CommandDispatcher();
        this.loadedRewards = new ArrayList<Reward>();
        this.cachedInventories = new HashMap<UUID, PlayerInventory>();

        load();
    }

    public void animate(Player player) {
        int cycleOneId = Bukkit.getScheduler().scheduleSyncRepeatingTask(getPlugin(), new Animation(plugin, player), 0, 5);

        while (true) {
            if (!Bukkit.getScheduler().isCurrentlyRunning(cycleOneId)) {


                break;
            }
        }
    }

    private void stashInv(Player player) {
        if (cachedInventories.containsKey(player.getUniqueId())) {
            player.sendMessage(ChatColor.RED + "Your inventory is already stashed!");

            return;
        }

        PlayerInventory pi = player.getInventory();

        cachedInventories.put(player.getUniqueId(), pi);

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
    }

    private void restoreInv(Player player) {
        if (!cachedInventories.containsKey(player.getUniqueId())) return;

        player.getInventory().clear();

        PlayerInventory pi = cachedInventories.get(player.getUniqueId());

        player.getInventory().setArmorContents(pi.getArmorContents());
        player.getInventory().setContents(pi.getContents());
        player.updateInventory();

        cachedInventories.remove(player.getUniqueId());
    }

    public Range getRange() {
        return Range.deserialize(plugin.getConfig().getConfigurationSection("range").getValues(true));
    }

    private void load() {
        FileConfiguration config = plugin.getConfig();
        ConfigurationSection section = config.getConfigurationSection("rewards");

        for (String key : section.getKeys(false)) {
            if (getLoadedRewards().size() >= 9) break;

            Reward reward = Reward.deserialize(section.getConfigurationSection(key).getValues(true));

            if (reward == null) continue;

            loadedRewards.add(reward);
        }

        if (getLoadedRewards().size() == 0) {
            Bukkit.getLogger().warning("No loaded rewards! Please re-configure plugin and reload. Disabling plugin...");
            Bukkit.getPluginManager().disablePlugin(getPlugin());

            return;
        }

        Bukkit.getLogger().info("Loaded " + getLoadedRewards().size() + " rewards.");
    }
}
