package me.tylergrissom.gazlikesmen.api;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2013-2017 Tyler Grissom
 */
public class Range implements ConfigurationSerializable {

    private int minimum, maximum;

    public int getMinimum() {
        return minimum;
    }

    public int getMaximum() {
        return maximum;
    }

    public Range(int minimum, int maximum) {
        this.minimum = minimum;
        this.maximum = maximum;
    }

    public static Range deserialize(Map<String, Object> map) {
        return new Range((Integer) map.get("min"), (Integer) map.get("max"));
    }

    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("min", getMinimum());
        map.put("max", getMaximum());

        return map;
    }

    public boolean contains(int comparator) {
        return comparator > minimum && comparator < maximum;
    }
}
