package me.tylergrissom.gazlikesmen.api;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

/**
 * Copyright (c) 2013-2017 Tyler Grissom
 */
public class Reward implements ConfigurationSerializable {

    private String name, command;
    private List<String> lore;
    private Material material;

    public String getName() {
        return name;
    }

    public String getCommand() {
        return command;
    }

    public List<String> getLore() {
        return lore;
    }

    public Material getMaterial() {
        return material;
    }

    public Reward(String name, String command, List<String> lore, Material material) {
        this.name = name;
        this.command = command;
        this.lore = lore;
        this.material = material;
    }

    public static Reward deserialize(Map<String, Object> map) {
        Object nameObj = map.get("Name");
        Object commandObj = map.get("Command");
        Object loreObj = map.get("Lore");
        Object materialObj = map.get("Display-Item");

        if (!(nameObj instanceof String)
                || !(commandObj instanceof String)
                || !(loreObj instanceof List)
                || !(materialObj instanceof String)
                || Material.getMaterial((String) materialObj) ==  null) {
            Bukkit.getLogger().warning("Incorrectly configured reward.");

            return null;
        }

        return new Reward(
                (String) nameObj,
                (String) commandObj,
                (List<String>) loreObj,
                Material.getMaterial((String) materialObj));
    }

    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("Name", getName());
        map.put("Command", getCommand());
        map.put("Lore", getLore());
        map.put("Display-Item", getMaterial());

        return map;
    }

    public ItemStack getIcon() {
        ItemStack itemStack = new ItemStack(getMaterial()); {
            ItemMeta meta = itemStack.getItemMeta();

            if (getName() != null) {
                meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', getName()));
            }

            if (getLore() != null) {
                List<String> oldLore = getLore();
                List<String> newLore = new ArrayList<String>();

                for (String string : oldLore) {
                    newLore.add(ChatColor.translateAlternateColorCodes('&', string));
                }

                meta.setLore(newLore);
            }

            itemStack.setItemMeta(meta);
        }

        return itemStack;
    }
}
