package me.tylergrissom.gazlikesmen.command;

import me.tylergrissom.gazlikesmen.GazLikesMenPlugin;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

/**
 * Copyright (c) 2013-2017 Tyler Grissom
 */
public class GazLikesMenCommand extends CommandBase {

    private GazLikesMenPlugin plugin;

    public GazLikesMenPlugin getPlugin() {
        return plugin;
    }

    public GazLikesMenCommand(GazLikesMenPlugin plugin) {
        this.plugin = plugin;
    }

    void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            PluginDescriptionFile pdFile = plugin.getDescription();

            sender.sendMessage(ChatColor.YELLOW + "Server is running GazLikesMen version " + pdFile.getVersion());
        } else {
            String arg = args[0];

            if (arg.equalsIgnoreCase("demo")) {
                if (sender instanceof Player) {
                    Player player = (Player) sender;

                    plugin.getApi().animate(player);
                } else {
                    sender.sendMessage(ChatColor.RED + "Only players can use that command.");
                }
            }
        }
    }
}
