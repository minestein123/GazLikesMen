package me.tylergrissom.gazlikesmen;

import me.tylergrissom.gazlikesmen.api.GazLikesMenApi;
import me.tylergrissom.gazlikesmen.api.Range;
import me.tylergrissom.gazlikesmen.api.Reward;
import me.tylergrissom.gazlikesmen.command.GazLikesMenCommand;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copyright (c) 2013-2017 Tyler Grissom
 */
public class GazLikesMenPlugin extends JavaPlugin {

    private GazLikesMenPlugin plugin;
    private GazLikesMenApi api;

    public GazLikesMenPlugin getPlugin() {
        return plugin;
    }

    public GazLikesMenApi getApi() {
        return api;
    }

    @Override
    public void onEnable() {
        plugin = this;
        api = new GazLikesMenApi(this);

        getConfig().options().copyDefaults(true);
        saveConfig();

        ConfigurationSerialization.registerClass(Reward.class);
        ConfigurationSerialization.registerClass(Range.class);

        getCommand("gazlikesmen").setExecutor(new GazLikesMenCommand(this));
    }
}
